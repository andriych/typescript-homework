import axios from 'axios';
import { Api, Config } from './enums';
import movieMapper from './helpers/map-movie';
import { IMovie, IMovieExtended } from './interfaces';

const getPopularMovies = async (page = 1): Promise<IMovie[]> => {
    const url = `${Api.BASE_URL}/movie/popular?api_key=${Config.API_KEY}&page=${page}`;

    const moviesExtended: IMovieExtended = await (await axios.get(url)).data;
    const movies: IMovie[] = movieMapper(moviesExtended);

    return movies;
};

const getTopRatedMovies = async (page = 1): Promise<IMovie[]> => {
    const url = `${Api.BASE_URL}/movie/top_rated?api_key=${Config.API_KEY}&page=${page}`;
    const moviesExtended: IMovieExtended = await (await axios.get(url)).data;
    const movies: IMovie[] = movieMapper(moviesExtended);

    return movies;
};

const getUpcomingMovies = async (page = 1): Promise<IMovie[]> => {
    const url = `${Api.BASE_URL}/movie/upcoming?api_key=${Config.API_KEY}&page=${page}`;
    const moviesExtended: IMovieExtended = await (await axios.get(url)).data;
    const movies: IMovie[] = movieMapper(moviesExtended);

    return movies;
};

const getMovieById = async (id: number): Promise<IMovie> => {
    const url = `${Api.BASE_URL}/movie/${id}?api_key=${Config.API_KEY}`;
    const movies: IMovie = await (await axios.get(url)).data;

    return movies;
};

const getMovieByName = async (name = ''): Promise<IMovie[]> => {
    const url = `${Api.BASE_URL}/search/movie?api_key=${Config.API_KEY}&query="${name}"*`;
    const moviesExtended: IMovieExtended = await (await axios.get(url)).data;
    const movies: IMovie[] = movieMapper(moviesExtended);

    return movies;
};

export {
    getPopularMovies,
    getTopRatedMovies,
    getUpcomingMovies,
    getMovieById,
    getMovieByName,
};
