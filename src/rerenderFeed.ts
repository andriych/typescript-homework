import Card from './components/card';
import { IMovie } from './interfaces';

const rerenderFeed = (posters: IMovie[]): HTMLElement => {
    const rootElement = document.getElementById('film-container');

    if (!rootElement) return document.createElement('div');

    rootElement.innerHTML = '';

    posters.forEach((poster) => {
        const card: HTMLElement = Card(poster);

        const cardWrapper: HTMLElement = document.createElement('div');
        cardWrapper.className = 'col-lg-3 col-md-4 col-12 p-2';
        cardWrapper.append(card);

        rootElement.append(cardWrapper);
        return card;
    });

    return rootElement;
};

export default rerenderFeed;
