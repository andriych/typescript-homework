import rerenderFeed from './rerenderFeed';
import { IMovie } from './interfaces';
import { Api, Config } from './enums';
import Card from './components/card';
import queryString from 'query-string';
import { getMoviesIdFromStorage } from './helpers/storage-helper';
import {
    getMovieById,
    getMovieByName,
    getPopularMovies,
    getTopRatedMovies,
    getUpcomingMovies,
} from './services';

interface IButtonsState {
    popular: boolean;
    topRated: boolean;
    upcoming: boolean;
    page: number;
}

const initializeButtons = (): IButtonsState => {
    const buttonsState: IButtonsState = {
        popular: true,
        topRated: false,
        upcoming: false,
        page: 1,
    };
    return buttonsState;
};

export async function render(): Promise<void> {
    const popularBtn = document.getElementById('popular');
    const topRatedBtn = document.getElementById('top_rated');
    const upcomingBtn = document.getElementById('upcoming');
    const navbarToggler = document.querySelector('.navbar-toggler');
    const loadMoreBtn = document.getElementById('load-more');
    const searchBtn = document.getElementById('submit');

    let buttonsState = initializeButtons();

    // Initial set of movies
    let data: IMovie[] = await getPopularMovies();

    // Popular movies
    popularBtn?.addEventListener('click', async () => {
        data = await getPopularMovies();
        buttonsState = { ...buttonsState, popular: true };
        rerenderFeed(data);
    });

    // Top rated movies
    topRatedBtn?.addEventListener('click', async () => {
        data = await getTopRatedMovies();
        buttonsState = { ...buttonsState, topRated: true };

        rerenderFeed(data);
    });

    // Upcoming movies
    upcomingBtn?.addEventListener('click', async () => {
        data = await getUpcomingMovies();
        buttonsState = { ...buttonsState, upcoming: true };

        rerenderFeed(data);
    });

    // Favorite movies
    navbarToggler?.addEventListener('click', async () => {
        const favoriteMoviesContainter =
            document.getElementById('favorite-movies');

        if (favoriteMoviesContainter) favoriteMoviesContainter.innerHTML = '';

        const favoriteMoviesId = getMoviesIdFromStorage();
        const movies: IMovie[] = [];

        for (const id of favoriteMoviesId) {
            const movie: IMovie = await getMovieById(id);
            movies.push(movie);
        }

        movies.forEach((movie) => {
            const cardWrapper: HTMLElement = document.createElement('div');
            cardWrapper.className = 'col-12 p-2';
            cardWrapper?.append(Card(movie));
            favoriteMoviesContainter?.append(cardWrapper);
        });

        rerenderFeed(data);
    });

    // Set random movie in the header
    window.addEventListener('load', () => {
        console.log('ddd', location.search);

        const randomMovie = document.getElementById('random-movie');
        const randomName = document.getElementById('random-movie-name');
        const randomDescription = document.getElementById(
            'random-movie-description'
        );

        const randomElement: IMovie =
            data[Math.floor(Math.random() * data.length)];

        if (randomName && randomDescription && randomMovie) {
            randomName.innerText = randomElement.original_title;
            randomDescription.innerText = randomElement.overview;
            randomMovie.style.background = `url(${Api.IMG_BASE_URL}${randomElement.backdrop_path}) no-repeat center`;
            randomMovie.style.backgroundSize = 'cover';
        }
    });

    // Change URL location.search
    function nextPage() {
        const parsed = queryString.parse(location.search);
        let currentPage = 1;

        if (parsed.page) currentPage = +parsed.page;

        currentPage++;

        const nextURL = `${Config.BASE_URL}?page=${currentPage}`;
        const nextTitle = 'My new page title';
        const nextState = { additionalInformation: 'Updated the URL' };

        window.history.pushState(nextState, nextTitle, nextURL);

        buttonsState = { ...buttonsState, page: currentPage };
    }

    // Load more movies
    loadMoreBtn?.addEventListener('click', async () => {
        nextPage();

        let movies: IMovie[] = [];

        if (buttonsState.popular)
            movies = await getPopularMovies(buttonsState.page);
        else if (buttonsState.topRated)
            movies = await getTopRatedMovies(buttonsState.page);
        else if (buttonsState.upcoming)
            movies = await getUpcomingMovies(buttonsState.page);

        data = data.concat(movies);

        rerenderFeed(data);
    });

    // Search by name
    searchBtn?.addEventListener('click', async () => {
        const input = document.getElementById('search') as HTMLInputElement;
        const movieName = input.value;

        const movies = await getMovieByName(movieName);
        if (movies) rerenderFeed(movies);
    });

    rerenderFeed(data);
}
