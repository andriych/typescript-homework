interface IMovie {
    id: number;
    backdrop_path: string | null;
    overview: string;
    release_date: string;
    original_title: string;
}

interface IMovieResult {
    poster_path: string | null;
    adult: boolean;
    overview: string;
    release_date: string;
    genre_ids: number[];
    id: number;
    original_title: string;
    original_language: string;
    title: string;
    backdrop_path: string | null;
    popularity: number;
    vote_count: number;
    video: boolean;
    vote_average: number;
}

interface IMovieExtended {
    page: number;
    results: IMovieResult[];
    total_results: number;
    total_pages: number;
}

export { IMovie, IMovieResult, IMovieExtended };
