const storeIdToLocalStorage = (id: number): number => {
    if (!localStorage.getItem('movies')) {
        localStorage.setItem('movies', JSON.stringify([id]));
        return id;
    }

    let movies = JSON.parse(localStorage.getItem('movies') || '[]');

    movies.push(id);
    movies = new Set(movies);
    localStorage.setItem('movies', JSON.stringify(movies));

    return id;
};

const getMoviesIdFromStorage = (): number[] => {
    if (!localStorage.getItem('movies')) {
        return [];
    }
    return JSON.parse(localStorage.getItem('movies') || '[]');
};

const removeMovieIdFromStorage = (id: number): number => {
    if (!localStorage.getItem('movies')) {
        return 0;
    }

    let movies: number[] = JSON.parse(localStorage.getItem('movies') || '[]');

    movies = movies.filter((el) => el !== id);

    localStorage.setItem('movies', JSON.stringify(movies));
    return id;
};

export {
    storeIdToLocalStorage,
    getMoviesIdFromStorage,
    removeMovieIdFromStorage,
};
