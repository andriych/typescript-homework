import { IMovie, IMovieExtended } from '../interfaces';

const movieMapper = (movie: IMovieExtended): IMovie[] => {
    const mapped = movie.results.map(
        (el) =>
            ({
                id: el.id,
                backdrop_path: el.backdrop_path,
                overview: el.overview,
                release_date: el.release_date,
                original_title: el.original_title,
            } as IMovie)
    );

    return mapped;
};

export default movieMapper;
