import { Api } from '../enums';
import {
    getMoviesIdFromStorage,
    removeMovieIdFromStorage,
    storeIdToLocalStorage,
} from '../helpers/storage-helper';

interface ICardProps {
    id: number;
    backdrop_path: string | null;
    overview: string;
    release_date: string;
}

const Card = ({
    id,
    backdrop_path,
    overview,
    release_date,
}: ICardProps): HTMLElement => {
    const card: HTMLElement = document.createElement('div');
    card.className = 'card shadow-sm';
    card.innerHTML = `
          <img
              src="${Api.IMG_BASE_URL}${backdrop_path}"
          />
          <svg
              xmlns="http://www.w3.org/2000/svg"
              stroke="red"
              fill="#ff000078"
              width="50"
              height="50"
              class="bi bi-heart-fill position-absolute p-2"
              viewBox="0 -2 18 22"
          >
              <path
                  fill-rule="evenodd"
                  d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
              />
          </svg>
          <div class="card-body">
              <p class="card-text truncate">
                  ${overview}
              </p>
              <div class="d-flex justify-content-between align-items-center">
                  <small class="text-muted">${release_date}</small>
              </div>
          </div>
    `;

    const saveBtn = card.querySelector('.bi');

    const isStored = getMoviesIdFromStorage().includes(id);

    if (isStored) {
        saveBtn?.setAttribute('fill', 'red');
    } else {
        saveBtn?.setAttribute('fill', '#ff000078');
    }

    saveBtn?.addEventListener('click', () => {
        const isStored = getMoviesIdFromStorage().includes(id);
        if (isStored) {
            saveBtn.setAttribute('fill', '#ff000078');
            removeMovieIdFromStorage(id);
        } else {
            saveBtn.setAttribute('fill', 'red');
            storeIdToLocalStorage(id);
        }
    });

    return card;
};

export default Card;
