const enum Config {
    API_KEY = 'c19a7bb7b61d4f38b9952c72447b798f',
    BASE_URL = 'http://localhost:9090/',
}

const enum Api {
    BASE_URL = 'https://api.themoviedb.org/3',
    IMG_BASE_URL = 'https://image.tmdb.org/t/p/original',
}

export { Config, Api };
